import React from 'react';
import { Route } from 'react-router';

import Home from 'screens/home';
import OrganizationEvents from 'screens/organization-events';
import Event from 'screens/event';
import EventManager from 'screens/event-manager';
import VoteSummary from 'screens/vote-summary';

import { ImproForm } from 'components/impro-form';
import VoteContainer from 'containers/vote';

export default [
    <Route path="/vote" component={VoteContainer} />,

    <Route path="/" component={Home} />,
    <Route path="/org/:orgId" component={OrganizationEvents} />,
    <Route path="/org/:orgId/event/:eventId" component={Event} />,
    <Route path="/org/:orgId/event/:eventId/manage" component={EventManager} />,
    <Route path="/org/:orgId/event/:eventId/vote" component={VoteSummary} />,
    // Steps are managed by the event state ;)

    <Route
        path="/impro"
        component={function() {
            return (
                <ImproForm
                    onSubmit={function(data) {
                        console.log(data);
                    }}
                />
            );
        }}
    />,
];
