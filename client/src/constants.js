export const StepStatus = {
    RUNNING: 'running',
    PENDING: 'pending',
    VOTING: 'voting',
};
