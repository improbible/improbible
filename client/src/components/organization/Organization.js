import React, { Component, PropTypes } from 'react';

const propTypes = {
    id: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
};

const defaultProps = {};

class Organization extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { image, name } = this.props;

        return (
            <div className="organizationTitle">
                <img src={image} alt={name} />
                <h3>{name}</h3>
            </div>
        );
    }
}

Organization.propTypes = propTypes;
Organization.defaultProps = defaultProps;

export default Organization;
