import React, { Component, PropTypes } from 'react';
import { getStepFromEvent, getStepById } from 'utils/selectors';
import { StepStatus } from 'constants';

const propTypes = {
    step: PropTypes.object.isRequired,
    startTimer: PropTypes.func.isRequired,
    endTimer: PropTypes.func.isRequired,
};

const defaultProps = {};

class ImproControls extends Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.isRunning = this.isRunning.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleStop = this.handleStop.bind(this);
        this.handleStartVote = this.handleStartVote.bind(this);
        this.handleStopVote = this.handleStopVote.bind(this);
    }
    isRunning() {
        const { step: { status, startedAt, duration } } = this.props;

        if (!startedAt) {
            return false;
        }

        return (
            status === StepStatus.RUNNING &&
            startedAt + duration * 1000 < new Date()
        );
    }
    handleStart() {
        this.props.startTimer();
    }
    handleStop() {
        this.props.endTimer();
    }
    handleStartVote() {
        this.props.startVote();
    }
    handleStopVote() {
        this.props.endVote();
    }
    render() {
        const { step: { status } } = this.props;

        return (
            <div>
                <h2 className="page-header">
                    Improvisation{' '}
                    <span className="label label-success">{status}</span>
                </h2>
                <div className="row">
                    <div className="col-xs-6">Timer</div>
                    <div className="btn-toolbar col-xs-6">
                        <div className="btn-group">
                            <button
                                className="btn btn-sm btn-primary"
                                onClick={this.handleStart}
                            >
                                <i className="fa fa-play" />
                            </button>
                            <button
                                className="btn btn-sm btn-danger"
                                onClick={this.handleStop}
                            >
                                <i className="fa fa-stop" />
                            </button>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-xs-6">Vote</div>
                    <div className="btn-toolbar col-xs-6">
                        <div className="btn-group">
                            <button
                                className="btn btn-sm btn-primary"
                                onClick={this.handleStartVote}
                            >
                                <i className="fa fa-play" />
                            </button>
                            <button
                                className="btn btn-sm btn-danger"
                                onClick={this.handleStopVote}
                            >
                                <i className="fa fa-stop" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ImproControls.propTypes = propTypes;
ImproControls.defaultProps = defaultProps;

export default ImproControls;
