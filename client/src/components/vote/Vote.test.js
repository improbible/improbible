import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Vote from './Vote';

it('renders correctly', () => {
    const tree = renderer.create(<Vote onSelect={() => {}} />).toJSON();

    expect(tree).toMatchSnapshot();
});

it('should select red', () => {
    const vote = shallow(<Vote onSelect={() => {}} />);

    vote.find('.left').simulate('click');

    expect(vote.state().team).toEqual('red');
});

it('should select blue', () => {
    const vote = shallow(<Vote onSelect={() => {}} />);

    vote.find('.right').simulate('click');

    expect(vote.state().team).toEqual('blue');
});
