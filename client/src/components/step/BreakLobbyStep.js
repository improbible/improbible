import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class BreakLobbyStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/6-entracte.png" />;
    }
}

BreakLobbyStep.propTypes = propTypes;
BreakLobbyStep.defaultProps = defaultProps;

export default BreakLobbyStep;
