import React, { Component, PropTypes } from 'react';
import categories from '../../../public/categories_fr.json';
import { StepStatus } from '../../constants';
import PieChart from 'react-minimal-pie-chart';
import { Vote } from './../vote';

import './ImproStep.less';

const propTypes = {
    category: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    nature: PropTypes.string.isRequired,
    nbOfPlayers: PropTypes.number.isRequired,
    status: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    voteDuration: PropTypes.number.isRequired,
    timerStartedAt: PropTypes.string.isRequired,
    registerVote: PropTypes.func.isRequired,
};

const defaultProps = {
    title: 'Ça m’en prend trois svp',
    nbOfPlayers: '5',
    category: 2,
    duration: 10,
    team: 'Tremblay',
    status: 'running',
    timerStartedAt: '',
};

function Timer({ seconds }) {
    return <h1>{formatTime(seconds)}</h1>;
}
function formatTime(duration) {
    var min = Math.floor(duration / 60);
    var sec = duration - min * 60;
    return (
        (min < 10 ? '0' + min.toString() : min.toString()) +
        ':' +
        (sec < 10 ? '0' + sec.toString() : sec.toString())
    );
}

class ImproStep extends Component {
    constructor(props) {
        super(props);

        this.state = {
            remainingDuration: 0,
        };

        this._timer = null;
        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.tick = this.tick.bind(this);
        this.computeRemainingDuration = this.computeRemainingDuration.bind(
            this
        );
    }

    componentDidUpdate(prevProps) {
        if (
            this.props.status !== prevProps.status &&
            this.isRunning(this.props.status)
        ) {
            this.startTimer();
        }
    }

    componentDidMount() {
        const { status } = this.props;

        if (this.isRunning(status)) {
            this.startTimer();
        }
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    computeRemainingDuration() {
        const { duration, timerStartedAt } = this.props;

        if (timerStartedAt) {
            const now = new Date().getTime();
            const startedAt = new Date(timerStartedAt).getTime();
            const durationMilis = 1000 * duration;

            const remaining = Math.floor(
                Math.max(startedAt + durationMilis - now, 0) / 1000
            );
            return remaining;
        }

        return 0;
    }

    startTimer() {
        this.stopTimer();

        const remainingTime = this.computeRemainingDuration();
        if (remainingTime > 0) {
            this.setState({
                remainingDuration: remainingTime,
            });

            this._timer = setInterval(this.tick, 1000);
        }
    }

    stopTimer() {
        if (this._timer) {
            clearInterval(this._timer);
        }
    }

    tick() {
        if (this.state.remainingDuration > 0) {
            this.setState(prevState => ({
                remainingDuration: this.computeRemainingDuration(),
            }));
        } else {
            this.stopTimer();
        }
    }

    isRunning(status) {
        return status === StepStatus.RUNNING;
    }

    render() {
        const { remainingDuration } = this.state;

        const {
            title,
            nature,
            nbOfPlayers,
            category,
            duration,
            team,
            status,
            registerVote,
        } = this.props;

        return (
            <div className="container" id="ImproStep">
                <div className="header">
                    <label>Titre : </label> <text>{title}</text>
                    <br />
                    <label>Nature : </label> <text>{nature}</text>
                    <br />
                    <label>Nombre des joueurs : </label>{' '}
                    <text>{nbOfPlayers}</text>
                    <br />
                    <label>Catégorie : </label>{' '}
                    <text>{categories[category].name}</text>
                    {/*<i className="fa fa-circle-info"></i> {categories[category].description}*/}
                    <br />
                    <label>Durée : </label> <text>{formatTime(duration)}</text>
                </div>

                {
                    <PieChart
                        className="bigCircle"
                        data={[{ value: 1, color: '#13dcb4' }]}
                        //data={[{ value: 1, color: 'grey' }]}
                        reveal={
                            remainingDuration > 0
                                ? remainingDuration * 100 / duration
                                : 100
                        }
                        startAngle={270}
                    >
                        <div className="smallCircle">
                            <Timer seconds={remainingDuration} />
                        </div>
                    </PieChart>
                }
                {status === StepStatus.VOTING && (
                    <Vote onSelect={registerVote} />
                )}
                <p>
                    {team == 'mixte' ? '' : 'Version'}
                    <br />
                    {team}
                </p>
            </div>
        );
    }
}

ImproStep.propTypes = propTypes;
ImproStep.defaultProps = defaultProps;

export default ImproStep;
