import React, { Component, PropTypes } from 'react';
import { getStepFromEvent, getStepById } from 'utils/selectors';
import cn from 'classname';

const propTypes = {
    steps: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
        })
    ).isRequired,
    selectedId: PropTypes.string,
    onSelect: PropTypes.func,
};

const defaultProps = {
    selectedId: '',
    onSelect: stepId => {},
};

class StepSelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedId: props.initialSelectedStepId,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleClick(evt) {
        this.props.onSelect(this.state.selectedId);
    }
    handleChange(evt) {
        this.setState({
            selectedId: evt.target.value,
        });
    }
    render() {
        const { steps, selectedId } = this.props;

        return (
            <div>
                <ul className="list-group">
                    {steps.map(step => {
                        const isActive = step.id === selectedId;

                        return (
                            <li
                                className={cn('list-group-item', {
                                    active: isActive,
                                })}
                                key={step.id}
                                onClick={() => {
                                    if (!isActive) {
                                        this.props.onSelect(step.id);
                                    }
                                }}
                            >
                                {step.type}
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

StepSelector.propTypes = propTypes;
StepSelector.defaultProps = defaultProps;

export default StepSelector;
