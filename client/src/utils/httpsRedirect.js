export function httpsRedirect() {
    if (('app.improbible.com' === window.location.host) && (window.location.protocol !== 'https:')) {
        window.location = window.location.toString().replace(/^http:/, 'https:');
    } else if ('improbible.gitlab.io' === window.location.host) {
        window.location = window.location.toString().replace(/^(http|https):\/\/improbible\.gitlab\.io/, 'https://app.improbible.com');
    }
}
