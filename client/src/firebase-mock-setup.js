import FirebaseMock from 'firebase-mock';

const mockDatabase = new FirebaseMock.MockFirebase();
const mockAuth = new FirebaseMock.MockFirebase();
const mockFirebase = FirebaseMock.MockFirebaseSdk(
    path => {
        return path ? mockDatabase.child(path) : mockDatabase;
    },
    () => {
        return mockAuth;
    }
);

export { mockDatabase, mockAuth, mockFirebase };
