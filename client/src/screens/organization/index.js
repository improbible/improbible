import React, { Component } from 'react';
import { connect } from 'react-redux';

class Organization extends Component {
    componentWillMount() {}

    render() {
        return (
            <div>
                <h1>Organization</h1>
                <ul>
                    {this.props.events.map(event => {
                        return (
                            <li
                                to={`/organization/${organization.id}/event/${event.id}`}
                            >
                                <Event key={event.id} {...event} />
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(Organization);
