import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    dataToJS,
    firebaseConnect,
    isEmpty,
    isLoaded,
} from 'react-redux-firebase';

import {
    BreakStep,
    FinalScoreStep,
    ImproStep,
    IntroStep,
    StarStep,
} from 'components/step';

import Summary from './Summary';

@firebaseConnect(props => [
    {
        path: '/events',
        queryParams: ['orderByChild=id', 'equalTo=' + props.params.eventId],
    },
])
@connect(({ firebase }) => ({
    eventById: dataToJS(firebase, '/events'),
}))
class VoteSummary extends Component {
    componentWillMount() {}

    render() {
        const eventId = this.props.params.eventId;
        if (!isLoaded(this.props.eventById)) {
            return null;
        } else if (isEmpty(this.props.eventById)) {
            return <span>No event</span>;
        }

        const event = this.props.eventById[eventId];
        const blueCount = event.vote ? event.vote.blue : 0;
        const redCount = event.vote ? event.vote.red : 0;

        return (
            <Summary
                blue={{ name: 'Bouchard', count: blueCount }}
                red={{ name: 'Tremblay', count: redCount }}
            />
        );
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(VoteSummary);
