import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    BreakStep,
    FinalScoreStep,
    ImproStep,
    IntroStep,
    StarStep,
    ImproRatingStep,
    BreakSubmitStep,
    NightInfoStep,
    BreakLobbyStep,
    NextEventStep,
} from 'components/step';
import eventService from 'services/event-service';

import { Vote } from 'components/vote';

import {
    dataToJS,
    firebaseConnect,
    isEmpty,
    isLoaded,
} from 'react-redux-firebase';

@firebaseConnect(props => [
    {
        path: '/events',
        queryParams: ['orderByChild=id', 'equalTo=' + props.params.eventId],
    },
    {
        path: '/steps',
    },
])
@connect(({ firebase }) => ({
    eventById: dataToJS(firebase, '/events'),
    stepById: dataToJS(firebase, '/steps'),
}))
class Event extends Component {
    componentWillMount() {}

    renderStep(step, eventId) {
        const { firebase } = this.props;

        console.log('loading step ' + step.type);

        if (step.type === 'intro') {
            return <IntroStep {...step} />;
        } else if (step.type === 'impro') {
            return (
                <ImproStep
                    {...step}
                    registerVote={team => {
                        eventService
                            .connect(firebase)
                            .registerVote({ eventId, team });
                    }}
                />
            );
        } else if (step.type === 'break') {
            return <BreakStep {...step} />;
        } else if (step.type === 'finalScore') {
            return <FinalScoreStep {...step} />;
        } else if (step.type === 'stars') {
            return <StarStep {...step} />;
        } else if (step.type === 'vote') {
            return <Vote {...step} />;
        } else if (step.type === 'rating') {
            return <ImproRatingStep {...step} />;
        } else if (step.type === 'break1') {
            return <BreakSubmitStep {...step} />;
        } else if (step.type === 'nightinfo') {
            return <NightInfoStep {...step} />;
        } else if (step.type === 'break2') {
            return <BreakLobbyStep {...step} />;
        } else if (step.type === 'nextevent') {
            return <NextEventStep {...step} />;
        } else {
            return 'Unable to render step';
        }
    }

    render() {
        const eventId = this.props.params.eventId;
        if (!isLoaded(this.props.eventById) || !isLoaded(this.props.stepById)) {
            return null;
        } else if (
            isEmpty(this.props.eventById) ||
            isEmpty(this.props.stepById)
        ) {
            return <span>No event</span>;
        }

        const event = this.props.eventById[eventId];
        const step = event.activeStep
            ? this.props.stepById[event.activeStep]
            : null;

        return <div>{step && this.renderStep(step, eventId)}</div>;
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(Event);
