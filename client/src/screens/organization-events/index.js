import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { Event } from 'components/event';

import {
    dataToJS,
    firebaseConnect,
    isEmpty,
    isLoaded,
} from 'react-redux-firebase';

@firebaseConnect(props => [
    {
        path: '/events',
        queryParams: [
            'orderByChild=organization',
            'equalTo=' + props.params.orgId,
        ],
    },
])
@connect(({ firebase }) => ({
    events: dataToJS(firebase, '/events'),
}))
class OrganizationEvents extends Component {
    componentWillMount() {}

    getEvents() {
        return Object.keys(this.props.events).map(id => {
            return this.props.events[id];
        });
    }

    render() {
        const { firebase, events } = this.props;
        const orgId = this.props.params.orgId;

        return (
            <div>
                {console.log(events)}
                <h1>Organization</h1>
                {/* TODO: geolocalize nearby events and suggest event based on location */}
                {!isLoaded(events) ? (
                    'Loading'
                ) : isEmpty(events) ? (
                    'Event list is empty'
                ) : (
                    <ul>
                        {this.getEvents().map(event => {
                            {
                                return (
                                    <li key={event.id}>
                                        <Link
                                            to={`/org/${orgId}/event/${event.id}`}
                                        >
                                            <Event {...event} />
                                        </Link>
                                    </li>
                                );
                            }
                        })}
                    </ul>
                )}
            </div>
        );
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(OrganizationEvents);
