import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    firebaseConnect,
    isLoaded,
    isEmpty,
    dataToJS,
} from 'react-redux-firebase';

@firebaseConnect(['/todos'])
@connect(({ firebase }) => ({
    todos: dataToJS(firebase, '/todos'),
}))
export default class Todos extends Component {
    static propTypes = {
        todos: PropTypes.object,
        firebase: PropTypes.object,
        text: PropTypes.string.isRequired,
    };

    render() {
        const { firebase, todos } = this.props;

        const handleAdd = () => {
            const { newTodo } = this.refs;
            firebase.push('/todos', { text: newTodo.value, done: false });
            newTodo.value = '';
        };

        const todosList = !isLoaded(todos)
            ? 'Loading'
            : isEmpty(todos)
              ? 'Todo list is empty'
              : Object.keys(todos).map((key, id) => (
                    <li key={key} id={id}>
                        {todos[key].text}
                    </li>
                ));

        return (
            <div>
                <h1>Todo</h1>
                <ul>{todosList}</ul>
                <input type="text" ref="newTodo" />
                <button onClick={handleAdd}>Add</button>
            </div>
        );
    }
}
