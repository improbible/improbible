import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import voteRepository from 'services/vote-repository';
import { Vote } from 'components/vote';

import { firebaseConnect, dataToJS } from 'react-redux-firebase';

@firebaseConnect(['/votes'])
@connect(({ firebase }) => ({
    votes: dataToJS(firebase, '/votes'),
}))
class VoteContainer extends Component {
    render() {
        const { votes, firebase } = this.props;
        const color = votes ? votes.color : null;

        return (
            <Vote
                lastVote={color}
                onSelect={color => {
                    voteRepository.connect(firebase).vote(color);
                }}
            />
        );
    }
}

export default VoteContainer;
