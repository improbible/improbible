function eventService(firebase) {
    function create({ name }) {
        const newEventRef = firebase
            .database()
            .ref(`events`)
            .push();

        return newEventRef.set({
            id: newEventRef.key,
            name
        });
    }

    function gotoStep({ eventId, stepId }) {
        return firebase
            .database()
            .ref(`events/${eventId}`)
            .update({
                activeStep: stepId,
            });
    }

    function registerVote({ eventId, team }) {
        return firebase
            .database()
            .ref(`events/${eventId}`)
            .transaction(event => {
                if (event) {
                    if (!event.vote) {
                        event.vote = {
                            red: 0,
                            blue: 0,
                        };
                    }

                    event.vote[team]++;
                }

                return event;
            });
    }
    return {
        create,
        gotoStep,
        registerVote,
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return eventService(firebase);
}

module.exports = {
    connect,
};
