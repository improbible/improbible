import EventService from './event-service';

import {mockFirebase, mockDatabase } from "../firebase-mock-setup";

const eventService = EventService.connect(mockFirebase);

it('should create an event', (done) => {
    eventService.create({name: "awesome event!"})
        .then((event) => {
            expect(Object.keys(event)).toHaveLength(2);
            expect(event.id).toBeDefined();
            expect(event.name).toBe("awesome event!");

            done();
        })
        .catch((err) => {
            console.log(err)
        })

    mockDatabase.flush()
})
